var CONFIG = arguments[0] || {},
    applyProperties = function(properties) {

	var button1Hidden = button2Hidden = false;

	$.dialogBoxTitleLabel.text = (properties.title) ? properties.title : "Whoops";
	$.dialogPicker.type = properties.type;
	if (properties.maxDate) {
		$.dialogPicker.maxDate = properties.maxDate;
	}
	var single = false;
	if (properties.values && properties.values.length > 0) {
		single = true;
		var data = [];
		for (var i = 0; i < properties.values.length; i++) {
			data.push(Ti.UI.createPickerRow({
				title : properties.values[i].name,
				code : properties.values[i].code
			}));
		};
		$.dialogPicker.add(data);
		$.dialogPicker.selectionIndicator = true;
		$.dialogPicker.setSelectedRow(0, 0);
	}
	$.dialogBoxLabel.text = (properties.label && properties.label.text !== undefined) ? properties.label.text : 'Click OK to continue.';
	$.dialogBoxButton1.text = (properties.button1 && properties.button1.text) ? properties.button1.text : 'OK';
	$.dialogBoxButton1.addEventListener('click', function() {
		$.dialogBoxButton1.opacity = 0.75;
		setTimeout(function() {
			$.dialogBoxButton1.opacity = 1;
			if (properties.button1 && properties.button1.clickEvent)
				properties.button1.clickEvent();
			close();
		}, 100);
	});
	$.dialogBoxButton2.text = (properties.button2 && properties.button2.text) ? properties.button2.text : 'OK';
	$.dialogBoxButton2.addEventListener('click', function() {
		$.dialogBoxButton2.opacity = 0.75;
		setTimeout(function() {
			$.dialogBoxButton2.opacity = 1;
			if (properties.button2 && properties.button2.clickEvent)
				properties.button2.clickEvent((single) ? $.dialogPicker.getSelectedRow(0, 1).code : $.dialogPicker.value);
			close();
		}, 100);
	});

},
    animatePulse = function(obj, callback, time_first, time_second) {
	var objWidth = parseInt(obj.width),
	    objHeight = parseInt(obj.height),
	    animation = Ti.UI.createAnimation({
		width : objWidth + 25,
		height : objHeight + 25,
		duration : time_first
	}),
	    animationHandler = function() {
		animation.removeEventListener('complete', animationHandler);
		animation.width = objWidth;
		animation.height = objHeight;
		animation.duration = time_second;
		obj.animate(animation);
		callback();
	};
	animation.addEventListener('complete', animationHandler);
	obj.animate(animation);
},
    animateShrink = function(obj, callback, time_first, time_second) {
	var objWidth = parseInt(obj.width),
	    objHeight = parseInt(obj.height),
	    animation = Ti.UI.createAnimation({
		width : objWidth,
		height : objHeight,
		duration : time_second
	}),
	    animationHandler = function() {
		animation.removeEventListener('complete', animationHandler);
		callback();
	};
	obj.width = "100%";
	obj.height = "100%";
	setTimeout(function(e) {
		animation.addEventListener('complete', animationHandler);
		obj.animate(animation);
	}, time_first);
},
    animateTV = function(obj, off, callback, time) {
	var objWidth = parseInt(obj.width),
	    objHeight = parseInt(obj.height),
	    animation = Ti.UI.createAnimation({
		height : (off) ? "5dp" : objHeight,
		duration : time
	}),
	    animationHandler = function() {
		animation.removeEventListener('complete', animationHandler);
		animation.width = (off) ? "100%" : objWidth;
		obj.animate(animation);
		callback();
	};
	if (!off) {
		obj.width = "100%";
		obj.height = "5dp";
	}
	setTimeout(function(e) {
		animation.addEventListener('complete', animationHandler);
		obj.animate(animation);
	}, (off) ? 0 : time);
},
    animateOpacity = function(obj, op, time) {
	var animation = Ti.UI.createAnimation({
		opacity : op,
		duration : time
	});
	obj.animate(animation);
},
    open = function() {

	/*
	 * Fade open default dialog for Android
	 */
	if (OS_ANDROID) {

		var picker = Ti.UI.createPicker({
			type : CONFIG.properties.type,
			value : new Date()
		});

		/*
		 * Open Titanium.UI.PICKER_TYPE_TIME
		 */
		if (CONFIG.properties.type === 0) {

			picker.showDatePickerDialog({
				value : new Date(),
				callback : function(_event) {
					if (_event.cancel) {

					} else {

						CONFIG.properties.button2.clickEvent(_event.value);

					}
				}
			});

			/*
			 * Open Titanium.UI.PICKER_TYPE_DATE
			 */
		} else if (CONFIG.properties.type === 1) {

			picker.showDatePickerDialog({
				value : new Date(),
				callback : function(_event) {
					if (_event.cancel) {

					} else {

						CONFIG.properties.button2.clickEvent(_event.value);

					}
				}
			});

		} else {

			if (CONFIG.properties.values && CONFIG.properties.values.length > 0) {

				var data = [];
				for (var i = 0; i < CONFIG.properties.values.length; i++) {
					data.push(CONFIG.properties.values[i].name);
				};
				data.push("Cancel");
				var opts = {
					title : (CONFIG.properties.title) ? CONFIG.properties.title : "Whoops",
					options : data
				};
				opts.cancel = (opts.options.length - 1);
				var dialog = Ti.UI.createOptionDialog(opts);
				dialog.addEventListener('click', function(_event) {

					if (_event.cancel) {
						return;
					}
					CONFIG.properties.button2.clickEvent(CONFIG.properties.values[_event.index].code);

				});

				dialog.show();

			}

		}

	}

	/*
	 * Fade open for IOS
	 */
	if (OS_IOS) {
		CONFIG.parentView.add($.dialogBoxWidget);
		animateOpacity($.dimmedBackground, 0.5, 250);
	}

},
    close = function() {
	animateOpacity($.dialogBox, 0, 200);
	setTimeout(function(e) {
		animateOpacity($.dimmedBackground, 0, 250);
		setTimeout(function(e) {
			CONFIG.parentView.remove($.dialogBoxWidget);
		}, 250);
	}, 200);
},
    init = function() {
	if (CONFIG.parentView) {
		if (CONFIG.properties) {
			applyProperties(CONFIG.properties);
		}
		open();
	}
};
init();
