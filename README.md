```
Alloy.createWidget('sobytes.crossplatform.picker', null, {
        parentView : $.baseWindow,
        properties : {
            type : Titanium.UI.PICKER_TYPE_PLAIN, /* (Titanium.UI.PICKER_TYPE_PLAIN|Titanium.UI.PICKER_TYPE_DATE|Titanium.UI.PICKER_TYPE_TIME) */
            values : [{
                "code" : "af",
                "name" : "Afrikaans"
            }, {
                "code" : "ar", 
                "name" : "Arabic"
            }],
            title : "Translation Language",
            minDate : null,
            maxDate : new Date(),
            label : {
                text : "Please select a language you would like to translate your articles into."
            },
            button1 : {
                text : 'Cancel',
                clickEvent : function(_event) {
                    console.log(_event);
                }
            },
            button2 : {
                text : 'Set',
                clickEvent : function(_event) {
                    console.log(_event);
                    Ti.App.Properties.setString('lang', _event);
                }
            }
        }
    });
```